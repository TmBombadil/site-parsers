#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from xlsxwriter.workbook import Workbook
from openpyxl import load_workbook

import pandas as pd

def search_product(url, driver):
    print(url)
    response = requests.get(url)
    if response.status_code == 404:
        return pd.DataFrame()
    driver.get(url)
    new_row = {}
    param_tab = driver.find_element_by_xpath('//*[@id="tab_3"]').click()
    parameters_name = driver.find_elements_by_xpath('//div[@class="techspecs--row-specification"]')
    parameters_value = driver.find_elements_by_xpath('//div[@class="techspecs--row-value"]')
    barcode = driver.find_elements_by_xpath('//div[@class="barcode-container"]/div')
    description_short = driver.find_elements_by_xpath('//div[@class="product-description--short-description"]')
    description_full = driver.find_elements_by_xpath('//div[@class="product-description--marketing-text"]')
    description_short_title = driver.find_elements_by_xpath('//div[@class="product-description--short-title"]')
    product_name = driver.find_elements_by_xpath('//div[@class="product-title"]/h1')
    if not len(parameters_name) or not len(parameters_value):
        return pd.DataFrame()
    name_list = ""
    value_list = ""
    if description_short:
        new_row['Краткое описание'] = description_short[0].text.replace('\n', '').strip()       
    if description_full:
        new_row['Описание'] = description_full[0].text.replace('\n', '').strip()
    if product_name:
        new_row['Наименование'] = product_name[0].text.replace('\n', '').strip()
    if barcode:
        new_row['Штрих-код'] = barcode[0].text.replace('\n', '').strip()
    for i in range(0, len(parameters_name), 1):
        if parameters_value[i].text:
            new_row[parameters_name[i].text.replace('\n', '').strip()] = parameters_value[i].text.replace('\n', '').strip()
        else:
            new_row[parameters_name[i].text.replace('\n', '').strip()] = "Да"
    value_list = value_list.replace('"', '')
    name_list = name_list.replace('"', '')
    icons_list = driver.find_elements_by_xpath('//div[@class="tab-content-symbols"]/div/div/div/div/span')
    img_list = driver.find_elements_by_xpath('//div[@class="article_thumbs"]/div/div/div/div[not(contains(@class, 360))]/a')
    icons_src = ""
    imgs_src = ""
    if icons_list:
        for icon in icons_list:
            icons_src += '|' + icon.get_attribute('style').split('"')[1]
    if img_list:
        for img in img_list:
            imgs_src += '|' + img.get_attribute('href')
    new_row['Icons'] = icons_src
    new_row['Images'] = imgs_src
    return pd.DataFrame([new_row])

def main():
    opts = Options()
    opts.set_headless()
    profile = webdriver.FirefoxProfile()
    profile.set_preference("javascript.enabled", False)
    driver = webdriver.Firefox(options=opts)
    driver.set_script_timeout(5)
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
    if source_file:
        data = pd.read_excel(source_file)
    articles = data["Item"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for row in articles:
        url = "https://www.makita.ru/product/" + str(row).lower() + ".html"
        new_row = search_product(url, driver)
        if new_row.empty:
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row["Item"] = [row]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
    writer = pd.ExcelWriter('result/makita.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/makita_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



