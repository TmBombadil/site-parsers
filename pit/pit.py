#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

warnings.filterwarnings("ignore")

def search_product(url, article):
    time.sleep(2)
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    further_link = tree.xpath('//div[contains(@class,"item_block")]')
    if not further_link:
        return pd.DataFrame()
    new_link = ''
    for link in further_link:
        data = link.xpath('.//div[@class="article_block"]/@data-value')[0]
        if data == article:
            new_link = link.xpath('.//*[@class="dark_link option-font-bold font_sm"]/@href')[0]
            break
    if not len(new_link):
        return pd.DataFrame()

    new_link = 'https://pittools.ru' + new_link
    print(new_link)
    time.sleep(2)
    response = requests.get(new_link, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    new_row = {}

    img_list = tree.xpath('//*[@class="product-detail-gallery__item text-center  product-detail-gallery__item--thmb"]/img/@data-src')

    if not img_list:
        img_list = tree.xpath('//*[@class="product-detail-gallery__link popup_link fancy"]/img/@data-src')

    imgs_src = ""
    if img_list:
        if isinstance(img_list, list):
            for img in img_list:
                imgs_src += '|https:' + img
        else:
            imgs_src = 'https:' + img_list[0]

    new_row['Images'] = imgs_src.strip(' \;\|')

    return pd.DataFrame([new_row])

def main():
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Артикул"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for i, row in enumerate(articles):
        if not row:
            continue
        row = str(row).strip(' ')
        #row = 'PH20-3.0'
        url = "https://pittools.ru/catalog/?q=" + row + "&s=Найти"
        print(i, url)
        new_row = search_product(url, row)
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Артикул'] = [row]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



