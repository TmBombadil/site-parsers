#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

warnings.filterwarnings("ignore")
strip_pattern = ' \:\|\r\n\t'

def search_product(url):
    time.sleep(1)
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    new_row = {}

    sidebar_parameters_name = tree.xpath('//table[@class="shop2-product-params"]//th/text()')
    sidebar_parameters_value = tree.xpath('//table[@class="shop2-product-params"]//td/text()')
    if len(sidebar_parameters_name):
        for i, name in enumerate(sidebar_parameters_name):
            new_row[name.strip(strip_pattern)] = sidebar_parameters_value[i].strip(strip_pattern)

    product_name = tree.xpath('//h1[contains(@class,"h1 widget-62 widget-type-h1 editorElement layer-type-widget")]/text()')
    if product_name:
        new_row['Наименование'] = product_name[0].strip(strip_pattern)

    description = tree.xpath('//div[contains(@class,"desc-area")]/text()')
    if description:
        new_row['Описание'] = ('').join(description).strip(strip_pattern)

    site_path = tree.xpath('//div[contains(@class,"widget-61 path widget-type-path editorElement layer-type-widget")]/a/text()')
    if site_path:
        category = '|'.join(site_path)
        new_row['Категория'] = category.split('Главная')[1].strip(strip_pattern)

    old_price = tree.xpath('//div[@class="product-r-side"]//div[@class="price-old"]//strong/text()')
    if old_price:
        new_row['Старая цена'] = old_price[0].replace(' ', '')

    cur_price = tree.xpath('//div[@class="product-r-side"]//div[contains(@class,"price-current")]//strong/text()')
    if cur_price:
        new_row['Цена'] = cur_price[0].replace(' ', '')

    article = tree.xpath('//div[@class="product-r-side"]//div[@class="shop2-product-article"]/text()')
    if article:
        new_row['Артикул'] = article[0].strip(strip_pattern)
        new_row['Срок изготовления'] = article[1].strip(strip_pattern)

    warranty = tree.xpath('//div[@class="product-r-side"]//div[@class="product-anonce"]/text()')
    if warranty:
        new_row['Гарантия производителя'] = warranty[0].strip(strip_pattern)


    img_list = tree.xpath('//div[contains(@class,"product-image-slick")]//img/@src')
    imgs_src = ""
    if img_list:
        for img in img_list:
            if imgs_src.find(img) == -1:
                imgs_src += '|https://ruchnoyinstrument.ru' + img

    new_row['Изображения'] = imgs_src.strip(' \;\|')
    return pd.DataFrame([new_row])

def main():
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    links = data["Ссылка"]

    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for i, url in enumerate(links):
        if not url:
            continue
        print(i, url)
        new_row = search_product(url)
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([url])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Ссылка'] = [url]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



