#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd

def search_product(url, driver):
    response = requests.get(url)
    if response.status_code == 404:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    further_link = tree.xpath('//div[contains(@class,"titleBox ")]/a/@href')
    if not further_link:
        return pd.DataFrame()
    print(further_link)
    driver.get(further_link[0])
    description_full = driver.find_elements_by_xpath('//div[@id="description"]/ul/li')
    new_row = {}
    if description_full:
        new_row['Описание'] = ""
        for row in description_full:
            new_row['Описание'] += row.text + '. '
    param_tab = driver.find_element_by_xpath('//*[@class="rCMain storeLocatorSearch"]')
    driver.execute_script("arguments[0].scrollIntoView();", param_tab)
    driver.find_element_by_xpath('//*[text()="Технические данные"]').click()
    parameters_name = driver.find_elements_by_xpath('//div[@class="attributesRow"]/div[1]')
    parameters_value = driver.find_elements_by_xpath('//div[@class="attributesRow"]/div[2]')
    product_name = driver.find_elements_by_xpath('//h1[@itemprop="name"]')
    ean = driver.find_elements_by_xpath('//*[@id="productEannum"]')
    if not len(parameters_name) or not len(parameters_value):
        return pd.DataFrame()
    name_list = ""
    value_list = ""
    if ean:
        new_row['EAN'] = ean[0].text.replace('\n', '').strip()
    if product_name:
        new_row['Наименование'] = product_name[0].text.replace('\n', '').strip()
    for i in range(0, len(parameters_name), 1):
        if parameters_value[i].text:
            new_row[parameters_name[i].text.replace('\n', '').strip()] = parameters_value[i].text.replace('\n', '').strip()
        else:
            new_row[parameters_name[i].text.replace('\n', '').strip()] = "Да"
    value_list = value_list.replace('"', '')
    name_list = name_list.replace('"', '')
    icons_list = driver.find_elements_by_xpath('//*[@class="symbolToolTip tooltip"]')
    img_list = driver.find_elements_by_xpath('//span[@class="artIcon"]/img')
    icons_src = ""
    imgs_src = ""
    if icons_list:
        for icon in icons_list:
            icons_src += '|' + icon.get_attribute('src')
    if img_list:
        for img in img_list:
            imgs_src += '|' + img.get_attribute('src')
    else:
        img = driver.find_elements_by_xpath('//*[@id="zoom1"]/img')[0]
        imgs_src += '|' + img.get_attribute('src')
    new_row['Icons'] = icons_src
    new_row['Images'] = imgs_src
    return pd.DataFrame([new_row])

def main():
    opts = Options()
    opts.set_headless()
    profile = webdriver.FirefoxProfile()
    profile.set_preference("javascript.enabled", False)
    driver = webdriver.Firefox(options=opts)
    driver.set_script_timeout(5)
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Номер"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for row in articles:
        row_correct = row.replace("'", "")
        url = "https://www.metabo.com/ru/index.php?lang=18&shp=11&cl=search&searchparam=" + str(row).lower()
        new_row = search_product(url, driver)
        if new_row.empty:
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Article'] = [row]
            #new_row['Correct_article'] = [row_correct]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
    writer = pd.ExcelWriter('result/metabo.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/metabo_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



