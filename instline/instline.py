#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

warnings.filterwarnings("ignore")
strip_pattern = ' \:\|\r\n\t'

def search_product(url):
    time.sleep(1)
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    new_row = {}

    img_list = tree.xpath('//div[@id="product-gallery"]//a/@href')

    imgs_src = ""
    if img_list:
        for img in img_list:
            imgs_src += '|https://instline.ru' + img
    else:
        img_list = tree.xpath('//div[@class="card-img"]//a/@href')
        if img_list:
            imgs_src = '|https://instline.ru' + img_list[0]
    new_row['Images'] = imgs_src.strip(' \;\|')

    sidebar_parameters_name = tree.xpath('//div[@class="card-info-left "]//li/div[contains(@class,"name")]/text()')
    sidebar_parameters_value = tree.xpath('//div[@class="card-info-left "]//li/div[contains(@class,"value")]/text()')
    if len(sidebar_parameters_name):
        for i, name in enumerate(sidebar_parameters_name):
            new_row[name.strip(strip_pattern)] = sidebar_parameters_value[i].strip(strip_pattern)

    product_name = tree.xpath('//h1[contains(@class,"category-name")]/text()')
    if product_name:
        new_row['Наименование'] = product_name[0]

    description = tree.xpath('//div[@id="product-description"]//text()')
    if description:
        new_row['Description'] = ('').join(description)

    site_path = tree.xpath('//div[@class="card-bread"]/a/text()')
    if site_path:
        category = '|'.join(site_path)
        new_row['Category'] = category.split('Главная')[1].strip(strip_pattern)

    price = tree.xpath('//div[@class="product-price"]/span/@data-price')
    if price:
        new_row['Price'] = price[0]

    product_kit = tree.xpath('//div[@class="card-info-right"]//li/text()')
    if product_kit:
        new_row['Delivery_Kit'] = ('\n').join(product_kit)
    return pd.DataFrame([new_row])

def main():
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    links = data["Ссылка"]

    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for i, url in enumerate(links):
        if not url:
            continue
        print(i, url)
        new_row = search_product(url)
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([url])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Ссылка'] = [url]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



