#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

warnings.filterwarnings("ignore")

def search_product(url, article, name):
    time.sleep(3)
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    further_link = tree.xpath('//*[@class="site-content-middle-in-2"]//ul/li')
    if not further_link:
        return pd.DataFrame()

    new_link = ''
    if len(further_link) == 1:
        new_link = further_link[0].xpath('.//a/@href')[0]
    else:
        for link in further_link:
            current_name = link.xpath('.//a/b/text()')[0]
            small_desc = link.xpath('.//a/b/text()')[0]
            if current_name.find(name) != -1 or current_name.find(article) != -1:
                new_link = link.xpath('.//a/@href')[0]
                break

    if not len(new_link):
        return pd.DataFrame()

    time.sleep(3)
    print('https://instart.ru' + new_link)
    response = requests.get('https://instart.ru' + new_link, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    new_row = {}

    img_list = tree.xpath('//div[@id="tovar_card2"]/div[@class="wrap"]/a/@href')

    imgs_src = ""
    if img_list:
        for img in img_list:
            imgs_src += '|https://instart.ru' + img
    new_row['Images'] = imgs_src.strip(' \;\|')
    description = tree.xpath('//div[@id="tovar_detail2"]/div//text()')
    if description:
        new_row['Description'] = description[0]

    site_path = tree.xpath('//div[@class="site-path"]/a/text()')
    category = '';
    if site_path:
        category = '|'.join(site_path)
    new_row['Category'] = category.split('Интернет магазин')[1].strip(' \;\|')

    site_price = tree.xpath('//li[@class="price"]//b/text()')
    if site_price:
        new_row['Site_price'] = site_price[0]

    return pd.DataFrame([new_row])

def main():
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, names=["Артикул", "Имя"], engine="openpyxl")
    articles = data["Артикул"]
    names = data["Имя"]

    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for i, row in enumerate(articles):
        if not row:
            continue
        article = str(row).strip(' ')
        name = str(names[i]).strip(' ').replace('  ', ' ')
        url = "https://instart.ru/search?search=" + row
        print(i, url)
        new_row = search_product(url, row, name)
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([row, name])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Артикул'] = [row]
            new_row['Имя_в_файле'] = [name]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



