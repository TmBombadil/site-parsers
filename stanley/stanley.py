#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

def search_product(url, article):
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)

    further_link = ''
    product_arr = tree.xpath('//div[contains(@class, "product-item__wrapper-inner")]')
    for product in product_arr:
        product_article = product.xpath('.//div[@class="product-item__sku"]/text()')[0]
        link = product.xpath('.//a[contains(@class,"product-item__title js-name")]/@href')[0]
        if link.find(str(article).lower()) != -1 or product_article.find(str(article)) != -1:
            further_link = link
    if not further_link:
        return pd.DataFrame()

    print(further_link)
    time.sleep(3)
    response = requests.get(further_link, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)

    new_row = {}

    parameters = tree.xpath('//*[@class="product-view__attribute-item"]')
    if not len(parameters):
        return pd.DataFrame()
    for i, row in enumerate(parameters):
        name = row.xpath('./dt[@class="product-attribute__title"]/text()')[0]
        value = row.xpath('./dd[@class="product-attribute__text"]//text()')
        if isinstance(value, list):
            new_row[name.strip(' \:\|')] = '<br>'.join(value)
            continue
        new_row[name.strip(' \:\|')] = value

    sidebar_parameters_name = tree.xpath('//div[@class="product-description__sidebar-item product-description__sidebar-item_measurement"]//div[@class="description-sidebar__info-subtitle"]/text()')
    sidebar_parameters_value = tree.xpath('//div[@class="product-description__sidebar-item product-description__sidebar-item_measurement"]//div[@class="description-sidebar__info-text"]/text()')
    if len(sidebar_parameters_name):
        for i, name in enumerate(sidebar_parameters_name):
            if name.find("Д х Ш х В") != -1:
                tmpParamsArr = name.split(' ')
                tmpValuesArr = sidebar_parameters_value[i].split(' ')
                unit = tmpParamsArr[5].strip(':')
                new_row['Длина, ' + unit] = tmpValuesArr[0]
                new_row['Ширина, ' + unit] = tmpValuesArr[2]
                new_row['Высота, ' + unit] = tmpValuesArr[4]
            else:
                new_row[name.strip(' \:\|')] = sidebar_parameters_value[i]

    description = tree.xpath('//*[@class="description__info-text"]//text()')
    if description:
        new_row['Описание'] = description[0]

    product_name = tree.xpath('//h1[contains(@class,"product-card__title")]/text()')
    if product_name:
        new_row['Наименование'] = product_name[0]

    price = tree.xpath('////div[@class="product-card__price product-card__price_description-info"]/span/text()')
    if price:
        new_row['Цена'] = price[0]

    img_list = tree.xpath('//img[contains(@class,"images-gallery__image images-gallery__image_thumbs")]/@src')
    if not img_list:
        img_list = tree.xpath('//img[@id="catalog_detail_image"]/@src')
    imgs_src = ""
    if img_list:
        imgs_src = '|'.join(img_list)
    new_row['Изображения'] = imgs_src.strip(' \;\|')

    advantages = tree.xpath('//*[contains(@class,"product-view__attribute-advantages")]//li/text()')
    adv_list = ""
    if advantages:
        for i, adv in enumerate(advantages):
            new_row['Преимущества' + str(i + 1)] = adv
    return pd.DataFrame([new_row])

def main():
    base_url = ["https://shop.stanley.ru/", "https://shop.dewalt.ru/"]
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Артикул"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    found_dewalt = pd.DataFrame()
    for i, row in enumerate(articles):
        if not row:
            continue
        dewalt_flag = False
        row = str(row).strip(' ')
        print("Row: " + str(i) + " Article: " + row)
        url = base_url[0] + "catalogsearch/result/?q=" + row
        new_row = search_product(url, row)
        if new_row.empty:
            print("empty")
            dewalt_flag = True
            url = base_url[1] + "catalogsearch/result/?q=" + row
            new_row = search_product(url, row)
            if new_row.empty:
                tmp = pd.DataFrame([row])
                not_found = pd.concat([not_found, tmp])
                continue
        new_row['Артикул'] = [row]
        if dewalt_flag:
            found_dewalt = pd.concat([found_dewalt, new_row], ignore_index=True, sort=False)
            continue
        found = pd.concat([found, new_row], ignore_index=True, sort=False)
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Stanley', index=False)
    found_dewalt.to_excel(writer, 'DeWalt', index=False)
    writer.save()

    writer_nf = pd.ExcelWriter('result/' + 'not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

warnings.filterwarnings("ignore")
main()

