#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
import subprocess


def call_url(sap, url, writer, nf_writer, driver):
    response = requests.get(url)
    if not response.content:
        nf_writer.writerow([sap])
        return 0
    tree = lh.document_fromstring(response.content)
    further_link = tree.xpath('//a[@class="link-to-product"]/@href')
    if not further_link:
        nf_writer.writerow([sap])
        return 0
    if not further_link[0]:
        nf_writer.writerow([sap])
        return 0
    driver.get(further_link[0])
    parameters_name = driver.find_elements_by_xpath('//div[@id="parameters-container"]/ul/li/span[@class="left"]')
    parameters_value = driver.find_elements_by_xpath('//div[@id="parameters-container"]/ul/li/span[@class="right"]')
    if not len(parameters_name) or not len(parameters_value):
        nf_writer.writerow([sap])
        return 0
    name_list = ""
    value_list = ""
    for i in range(0, len(parameters_name), 1):
        if i == 0:
            name_list += parameters_name[i].text.replace('\n', '').strip()
            value_list += parameters_value[i].text.replace('\n', '').strip()
        else:
            name_list += '|' + parameters_name[i].text.replace('\n', '').strip()
            try:
                value = parameters_value[i].text.replace('\n', '').strip().encode('ISO-8859-16').decode('cp1251')
                value_list += '|' + value
            except Exception as e:
                value_list += '|' + parameters_value[i].text.replace('\n', '').strip()
    value_list = value_list.replace('"', '')
    name_list = name_list.replace('"', '')
    icons_list = driver.find_elements_by_xpath('//div[@class="clearfix pictograms"]/img')
    img_list = driver.find_elements_by_xpath('//div[@class="fotorama__nav__frame fotorama__nav__frame--thumb"]/div/img')
    icons_src = ""
    imgs_src = ""
    if icons_list:
        for icon in icons_list:
            icons_src += '|' + icon.get_attribute('src')
    if img_list:
        for img in img_list:
            imgs_src += '|' + img.get_attribute('src')
    try:
        print([sap, name_list, value_list, icons_src, imgs_src])
        writer.writerow([sap, name_list.replace('"', ''), value_list.replace('"', ''), icons_src, imgs_src])
    except Exception as e:
        print("error", url)


def main():
    opts = Options()
    opts.set_headless()
    profile = webdriver.FirefoxProfile()
    profile.set_preference("javascript.enabled", False)
    driver = webdriver.Firefox(options=opts)
    driver.set_script_timeout(5)
    file = open(argv[1] + '.csv', 'r', encoding='cp1251')
    reader = csv.reader(file)
    write_file = open(argv[1] + '_articles.csv', 'a', encoding='cp1251')
    not_found = open(argv[1] + '_articles_not_found.csv', 'a', encoding='cp1251')
    writer = csv.writer(write_file)
    nf_writer = csv.writer(not_found)
    urls = []
    for i, row in enumerate(reader):
        url = 'https://katalog.grupatopex.com/ru/catalogsearch/result/?q=' + row[0]
        print(i, url)
        try:
            call_url(row[0], url, writer, nf_writer, driver)
        except Exception as e:
            driver = webdriver.Firefox(options=opts)
            driver.set_script_timeout(5)
    driver.close()
main()
subprocess.Popen(['python', 'split.py'])
