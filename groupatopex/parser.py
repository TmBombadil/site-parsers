#!/usr/bin/env python
import requests
import csv
import time
import sys
import math
import subprocess

files = ['verto', 'neo']

for file in files:
    subprocess.Popen(['python', 'grupatopex.py', file])
    print(file + " in progress...")
