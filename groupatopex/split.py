#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
import time
from sys import argv
import os


def write_data(writer, reader):
    for row in reader:
        wrrow = {}
        wrrow['Article'] = row[0]
        wrrow['Icons'] = row[3]
        wrrow['Images'] = row[4]
        params = row[1].split('|')
        values = row[2].split('|')
        for i in range(0, len(params), 1):
            wrrow[params[i]] = values[i]
        writer.writerow(wrrow)


def add_headers(reader):
    headers = ['Article', 'Icons', 'Images']
    for row in reader:
        parameters = row[1].split('|')
        for p in parameters:
            if p not in headers:
                headers.append(p)
    return headers


def main():
    directory = os.getcwd()
    files = os.listdir(directory)
    for file in files:
        if file.find("_articles") != -1:
            read_file = open(file, 'r', encoding='cp1251')
            reader = csv.reader(file)
            headers = add_headers(reader)
            write_file = open(file.split("_articles")[0] + "_final.csv", 'w', encoding='cp1251')
            writer = csv.DictWriter(write_file, fieldnames=headers)
            writer.writeheader()
            write_data(writer, reader)
main()
