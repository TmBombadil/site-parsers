#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd

def search_product(url, driver, article):
    time.sleep(5)
    driver.get(url)
    further_link = driver.find_elements_by_xpath('//yass-h3/a')
    if not further_link:
        return pd.DataFrame()
    print(url)
    driver.get(further_link[0].get_attribute('href'))
    time.sleep(7)
    new_row = {}
    parameters_name = driver.find_elements_by_xpath('//*[@class="styled pricelist"]//th')
    table_length = len(driver.find_elements_by_xpath('//*[@class="styled pricelist"]//tbody/tr'))
    if not len(parameters_name) or not table_length:
        return pd.DataFrame()
    for i in range(table_length):
        parameters_table = driver.find_elements_by_xpath('//*[@class="styled pricelist"]//tbody/tr[' + str(i + 1) +']/td')
        if parameters_table[0].text == article:
            parameters_value = parameters_table
            break
    for i, name in enumerate(parameters_name):
        new_row[name.text] = parameters_value[i].text
    info = driver.find_elements_by_xpath('//div[@class="col2"]//div[@class="item-techinfo"]//h2')
    for j, name in enumerate(info):
        info_full = driver.find_elements_by_xpath('//*[@class="bullet-list"][' + str(i + 1) +']/li')
        if info_full:
            for i, value in enumerate(info_full):
                if i == 0:
                    new_row[name.text] = ""
                new_row[name.text] += value.text + '. '
    product_name = driver.find_elements_by_xpath('//h1')
    if product_name:
        new_row['Наименование'] = product_name[0].text.replace('\n', '').strip()
    description = driver.find_elements_by_xpath('//div[@class="item-description"]/p')
    if description:
        new_row['Дополнительное описание'] = description[0].text.replace('\n', '').strip()
    #icons_list = driver.find_elements_by_xpath('//*[@class="symbolToolTip tooltip"]')
    img_list = driver.find_elements_by_xpath('//div[@class="col2"]//img[not(contains(@title, "корз"))]')
    #icons_src = ""
    imgs_src = ""
    '''if icons_list:
        for icon in icons_list:
            icons_src += '|' + icon.get_attribute('src')'''
    if img_list:
        for img in img_list:
            imgs_src += '|' + img.get_attribute('src')
    #new_row['Icons'] = icons_src
    new_row['Images'] = imgs_src
    return pd.DataFrame([new_row])

def main():
    opts = Options()
    opts.set_headless()
    profile = webdriver.FirefoxProfile()
    profile.set_preference("javascript.enabled", False)
    driver = webdriver.Firefox(options=opts)
    driver.set_script_timeout(5)
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Артикул"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for row in articles:
        if not row or math.isnan(row):
            continue
        row = str(int(row))
        url = "https://gedore-tool.ru/search.html?searchid=2237269&text=" + row + "&web=0#"
        try:
            new_row = search_product(url, driver, row)
        except:
            continue
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Article'] = [row]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/Gedore.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/Gedore_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

main()



