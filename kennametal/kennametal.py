#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
import time
import subprocess
import sys

def call_url(sap, url, writer, nf_writer):
    response = requests.get(url)
    if not response.content:
        nf_writer.writerow([sap])
        return
    tree = lh.document_fromstring(response.content)
    title = tree.xpath('//div[@class="product-name-content"]/h1/text()')
    subtitle = tree.xpath('//div[@class="product-name-content"]/h3/text()')
    if not title or not subtitle:
        nf_writer.writerow([sap])
        return
    main_title = subtitle[0].strip() + ' ' + title[0].strip()
    icon_list = tree.xpath('//li[@class="icon"]/img/@src')
    if icon_list:
        icon_src_list = '|'.join(icon_list)
    #print(icon_src_list)
    try:
        writer.writerow([sap, main_title, icon_src_list])
    except Exception as e:
        print("error", url)

def main():
    read_file = open('Articles.csv', 'r', encoding='cp1251')
    reader = csv.reader(read_file)
    headers = ["SAP", "Combined_title", "icons_links"]
    write_file = open('Articles_rus.csv', 'w', encoding='cp1251')
    writer = csv.writer(write_file)
    writer.writerow(headers)
    not_found = open('Articles_not_found.csv', 'w', encoding='cp1251')
    nf_writer = csv.writer(not_found)
    urls = []
    try:
        for i, row in enumerate(reader):
            if i < int(sys.argv[1]) or i > int(sys.argv[2]):
                continue
            url = 'https://www.kennametal.com/ru/ru/site-search.html?query=' + row[0]
            call_url(row[0], url, writer, nf_writer)
    except Exception as e:
        print(e.message)
    finally:
        read_file.close()
        write_file.close()
        not_found.close()

main()

    subprocess.Popen(['python3.6', 'grupatopex.py', file])