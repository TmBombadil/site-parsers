#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import csv
import time
import sys
import math
import subprocess


file = open('Articles.csv', 'r', encoding='cp1251')

count = sum(1 for _ in file)
parts = 10
part_size = math.ceil(count / parts)

for i in range(1, parts):
    print(i)
    subprocess.Popen(['python', 'kennametal.py', str(part_size * i), str(part_size * (i + 1)), ' > /dev/null'])

