#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings
warnings.filterwarnings("ignore")
strip_pattern = ' \:\|\r\n\t'

def search_product(driver, article):
    time.sleep(2)
    driver.get("https://digitopelectric.ru/")
    driver.find_element_by_xpath('//input[@id="story"]').send_keys(article)
    time.sleep(1)
    driver.find_element_by_xpath('//button[@class="btn q_search_btn"]').click()
    time.sleep(3)
    xpath_product = '//div[@class="inner-content__new-item-info"]//a[contains(text(), "' + article.upper() + '")]'
    product_block = False
    if len(driver.find_elements_by_xpath(xpath_product)):
        elem = driver.find_element_by_xpath(xpath_product)
        if elem.get_attribute('href').find('katalog') != -1:
            product_block = elem

    if not product_block:
        product_blocks = driver.find_elements_by_xpath('//div[@class="inner-content__new-item-info"]//a')
        if len(product_blocks):
            for elem in product_blocks:
                if elem.text.find(article.upper()) != -1 and elem.get_attribute('href').find('katalog') != -1:
                    product_block = elem

    if not product_block:
        return pd.DataFrame()
    url = product_block.get_attribute('href')
    print(url)
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    new_row = {}

    description = tree.xpath('//div[contains(@class, "description")]//text()')
    if description:
        new_row['Описание'] = ('').join(description)

    img_list = tree.xpath('//div[contains(@class, "gallery")]//a/@href')

    imgs_src = ""
    if img_list:
        for img in img_list:
            imgs_src += '|https://digitopelectric.ru' + img
    else:
        img_list = tree.xpath('//div[@class="card-img"]//a/@href')
        if img_list:
            imgs_src = '|https://digitopelectric.ru' + img_list[0]
    new_row['Изображения'] = imgs_src.strip(' \;\|')

    price = tree.xpath('//div[@class="show_price"]//text()')
    if price:
        new_row['Цена'] = price[0]

    product_name = tree.xpath('//div[@class="buttons__name-content"]//text()')
    if product_name:
        new_row['Наименование'] = product_name[0].strip(strip_pattern)

    parameters_list = tree.xpath('//div[contains(@class,"details")]//text()')
    if len(parameters_list):
        for parameter in parameters_list:
            ar_param = parameter.split(':')
            if len(ar_param) > 1:
                new_row[ar_param[0].strip()] = ar_param[1].strip()

    return pd.DataFrame([new_row])

def main():
    opts = Options()
    opts.set_headless()
    profile = webdriver.FirefoxProfile()
    profile.set_preference("javascript.enabled", False)
    driver = webdriver.Firefox(options=opts)
    driver.set_script_timeout(5)
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Артикул"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for row in articles:
        if not row:
            continue
        row = row.strip()
        print(row)
        new_row = search_product(driver, row)

        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Артикул'] = [row]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
        #break
    writer = pd.ExcelWriter('result/' + brand +'.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()
    driver.close()

main()
