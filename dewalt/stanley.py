#!/usr/bin/env python
import requests
from lxml import html as lh
from lxml import etree
import csv
from sys import argv
import time, re, random, os, math

from xlsxwriter.workbook import Workbook
import openpyxl
from openpyxl import load_workbook

import pandas as pd
import warnings

def search_product(url, article):
    response = requests.get(url, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)
    further_link_arr = tree.xpath('//a[contains(@class,"product-item__title js-name")]/@href')
    print(further_link_arr)
    further_link = ''
    for link in further_link_arr:
        if link.find(str(article).lower()) != -1:
            further_link = link
    if not further_link:
        return pd.DataFrame()

    print(further_link)
    time.sleep(3)
    response = requests.get(further_link, verify=False)
    if not response.content:
        return pd.DataFrame()
    tree = lh.document_fromstring(response.content)

    new_row = {}

    parameters = tree.xpath('//*[@class="product-view__attribute-item"]')
    if not len(parameters):
        return pd.DataFrame()
    for i, row in enumerate(parameters):
        name = row.xpath('./dt[@class="product-attribute__title"]/text()')[0]
        value = row.xpath('./dd[@class="product-attribute__text"]//text()')
        if isinstance(value, list):
            new_row[name.strip(' \:\|')] = '<br>'.join(value)
            continue
        new_row[name.strip(' \:\|')] = value

    sidebar_parameters_name = tree.xpath('//div[@class="product-description__sidebar-item product-description__sidebar-item_measurement"]//div[@class="description-sidebar__info-subtitle"]/text()')
    sidebar_parameters_value = tree.xpath('//div[@class="product-description__sidebar-item product-description__sidebar-item_measurement"]//div[@class="description-sidebar__info-text"]/text()')
    if len(sidebar_parameters_name):
        for i, name in enumerate(sidebar_parameters_name):
            if name.find("Д х Ш х В") != -1:
                tmpParamsArr = name.split(' ')
                tmpValuesArr = sidebar_parameters_value[i].split(' ')
                unit = tmpParamsArr[5].strip(':')
                new_row['Длина, ' + unit] = tmpValuesArr[0]
                new_row['Ширина, ' + unit] = tmpValuesArr[2]
                new_row['Высота, ' + unit] = tmpValuesArr[4]
            else:
                new_row[name.strip(' \:\|')] = sidebar_parameters_value[i]

    description = tree.xpath('//*[@class="description__info-text"]//text()')
    if description:
        new_row['Описание'] = description[0]

    product_name = tree.xpath('//h1[contains(@class,"product-card__title")]/text()')
    if product_name:
        new_row['Наименование'] = product_name[0]
    
    img_list = tree.xpath('//img[contains(@class,"images-gallery__image images-gallery__image_thumbs")]/@src')
    if not img_list:
        img_list = tree.xpath('//img[@id="catalog_detail_image"]/@src')
    imgs_src = ""
    if img_list:
        imgs_src = '|'.join(img_list)
    new_row['Изображения'] = imgs_src.strip(' \;\|')

    advantages = tree.xpath('//*[contains(@class,"product-view__attribute-advantages")]//li/text()')
    adv_list = ""
    if advantages:
        for i, adv in enumerate(advantages):
            new_row['Преимущества' + str(i + 1)] = adv
    return pd.DataFrame([new_row])

def main():
    files = os.listdir()
    for file in files:
        if file.find(".xls") != -1:
            source_file = file
            brand = file.split('.')[0]
    if source_file:
        data = pd.read_excel(source_file, engine="openpyxl")
    articles = data["Артикул"]
    not_found = pd.DataFrame()
    found = pd.DataFrame()
    for i, row in enumerate(articles):
        if not row:
            continue
        row = str(row).strip(' ')
        print(i)
        url = "https://shop.stanley.ru/catalogsearch/result/?q=" + row
        new_row = search_product(url, row)
        if new_row.empty:
            print("empty")
            tmp = pd.DataFrame([row])
            not_found = pd.concat([not_found, tmp])
        else:
            new_row['Артикул'] = [row]
            found = pd.concat([found, new_row], ignore_index=True, sort=False)
#            break
    writer = pd.ExcelWriter('result/' + brand +'2.xlsx')
    found.to_excel(writer, 'Sheet1', index=False)
    writer.save()
    writer_nf = pd.ExcelWriter('result/' + brand +'_not_found.xlsx')
    not_found.to_excel(writer_nf, 'Sheet1', index=False)
    writer_nf.save()

warnings.filterwarnings("ignore")
main()

